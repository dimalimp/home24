import PropsTypes from 'prop-types';
import './Egg.scss';

const Egg = ({ colorEgg, onClick, id }) => {
    return (<div onClick={ onClick } className={ `egg intro__egg egg_${colorEgg}` }>
        <div className="egg__number">I am { id }</div>
    </div>)
}

Egg.PropsTypes = {
    colorEgg: PropsTypes.string,
}

export default Egg;