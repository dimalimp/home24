import { Component } from 'react';
import PropsTypes from 'prop-types';
import './Button.scss';

class Button extends Component {
    state = {
        clickBtn: 0,
        isEven: "button_green",
    }

    click = () => {
        this.setState((prevState) => ({
            clickBtn: prevState.clickBtn + 1,
            isEven: prevState.clickBtn % 2 === 1 ? "button_green" : "",
        }))
    }

    render() {
        const { className, text } = this.props
        const { clickBtn, isEven } = this.state
        return (
            <button onClick={ this.click } className={ `button${className || ""} ${isEven}` }>{ text }{ clickBtn }</button>
        )
    }
}

Button.PropsTypes = {
    className: PropsTypes.string,
}

export default Button;