import { Component } from 'react';
import Egg from '../Egg/Egg';
import './Intro.scss';

class Intro extends Component {
    state = {
        arr: [
            { colorEgg: 'gray', id: 1 },
            { colorEgg: 'yellow', id: 2 },
            { colorEgg: 'black', id: 3 },
            { colorEgg: 'yellow', id: 4 },
            { colorEgg: 'yellow', id: 5 },
            { colorEgg: 'orange', id: 6 },
            { colorEgg: 'orange', id: 7 },
            { colorEgg: 'yellow', id: 8 },
        ],
    };

    deleteElement = (id) => {
        this.setState(prevState => ({
            arr: prevState.arr.filter(el => el.id !== id)
        }))
    }

    render() {
        const { arr } = this.state
        return (
            <section className="intro">
                <div className="container">
                    <div className="intro__inner">
                        { arr.map((item) => (
                            <Egg
                                key={ item.id }
                                onClick={ () => { this.deleteElement(item.id) } }
                                colorEgg={ item.colorEgg }
                                id={ item.id }
                            />
                        )) }

                    </div>
                </div>
            </section>
        )
    }
}

export default Intro;